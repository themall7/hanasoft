# TeamPlayer

The TeamPlayer is API to process teams and players.

## Installation

prerequisite  
php 8  
laravel 8

1. Clone source code
```bash
git clone https://themall7@bitbucket.org/themall7/hanasoft.git
```
2. Configure .env file (Shared via email)
3. composer install
4. migrate/seed
```
php artisan migrate:fresh --seed
```
5. generate swagger
```
php artisan l5-swagger:generate
```
6. Serving Laravel or run web server
```
sudo php artisan serve --port=80 --host=0.0.0.0
```
7. access swagger api  
http://{server_name}:{port}/api/documentation

8. Authorize
After creating a user or login, the api will return the token then copy & paste it to Authorize popup.  
e.g.
```
{
  "status": "Success",
  "message": null,
  "data": {
    "token": "10|EA7n117NA1GUKWGtwBaJ9ESidnOpF0vWDsYNueod"
  }
}
```
EA7n117NA1GUKWGtwBaJ9ESidnOpF0vWDsYNueod is the token to authorize.

## Note
We can use the database in shawnseo.com or setup new database in separated server.

Please make sure to update tests as appropriate.
