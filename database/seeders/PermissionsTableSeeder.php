<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guard_name = null;
        $api_user_permission = [];
        $api_admin_permission = [];
        $api_user = Role::where(['name' => 'api_user'])->first();
        $api_admin = Role::where(['name' => 'api_admin'])->first();
        $api_user_permission[] = Permission::create(['name' => 'team-list', 'guard_name' => $guard_name]);
        $api_admin_permission[] = Permission::create(['name' => 'team-create', 'guard_name' => $guard_name]);
        $api_admin_permission[] = Permission::create(['name' => 'team-update', 'guard_name' => $guard_name]);
        $api_admin_permission[] = Permission::create(['name' => 'team-delete', 'guard_name' => $guard_name]);
        $api_user_permission[] = Permission::create(['name' => 'player-list', 'guard_name' => $guard_name]);
        $api_admin_permission[] = Permission::create(['name' => 'player-create', 'guard_name' => $guard_name]);
        $api_admin_permission[] = Permission::create(['name' => 'player-update', 'guard_name' => $guard_name]);
        $api_admin_permission[] = Permission::create(['name' => 'player-delete', 'guard_name' => $guard_name]);
//         $permission->syncRoles($api_user);
//         $permission->syncRoles($api_user);
        $api_user->syncPermissions($api_user_permission);
        $api_admin->syncPermissions(array_merge($api_user_permission, $api_admin_permission));
    }
}
