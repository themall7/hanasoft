<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $guard_name = null;
        $api_user = Role::create(['name' => 'api_user', 'guard_name' => $guard_name]);
        $api_admin = Role::create(['name' => 'api_admin', 'guard_name' => $guard_name]);
    }
}
