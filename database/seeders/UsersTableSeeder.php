<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(['name' => 'shawn', 'email' => 'me@shawnseo.com', 'password' => bcrypt('password')]);
        $user->assignRole('api_user');
        $admin = User::create(['name' => 'admin', 'email' => 'admin@shawnseo.com', 'password' => bcrypt('password')]);
        $admin->assignRole('api_admin');
    }
}
