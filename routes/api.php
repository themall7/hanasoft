<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\TeamController;
use App\Http\Controllers\Api\VersionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/register', [AuthController::class, 'register']);

Route::post('/auth/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum'], 'prefix' => '/auth'], function () {
    Route::get('/me', [AuthController::class, 'me']);

    Route::post('/logout', [AuthController::class, 'logout']);
});

/////////////////////////////////////////////
// Teams
/////////////////////////////////////////////
Route::group(['middleware' => [], 'namespace' => 'App\Http\Controllers\Api', 'prefix' => '/teams'], function () {
    Route::get('', 'TeamController@index');
    Route::get('/{id}/players', 'PlayerController@showTeamPlayers')->where('id', '[0-9]+');
    Route::get('/{name}/players', 'PlayerController@getTeamPlayersByName');
});
Route::group(['middleware' => ['auth:sanctum'], 'namespace' => 'App\Http\Controllers\Api', 'prefix' => '/teams'], function () {
    Route::get('/{id}', 'TeamController@show')->where('id', '[0-9]+');
    Route::get('/{name}', 'TeamController@getByName');
    Route::post('', 'TeamController@store');
    Route::put('/{id}', 'TeamController@update');
    Route::delete('/{id}', 'TeamController@destroy');
});
    
/////////////////////////////////////////////
// Players
/////////////////////////////////////////////
Route::group(['middleware' => [], 'namespace' => 'App\Http\Controllers\Api', 'prefix' => '/players'], function () {
    Route::get('/{id}', 'PlayerController@show')->where('id', '[0-9]+');
    Route::get('/{name}', 'PlayerController@getByName');
});
Route::group(['middleware' => ['auth:sanctum'], 'namespace' => 'App\Http\Controllers\Api', 'prefix' => '/players'], function () {
    Route::get('', 'PlayerController@index');
    Route::post('', 'PlayerController@store');
    Route::put('/{id}', 'PlayerController@update');
    Route::delete('/{id}', 'PlayerController@destroy');
});

/////////////////////////////////////////////
// App | Versions
/////////////////////////////////////////////
Route::group(['namespace' => 'App\Http\Controllers\Api', 'prefix' => '/versions'], function () {
    Route::get('/', 'VersionController@index');
});