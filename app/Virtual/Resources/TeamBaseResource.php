<?php

namespace App\Http\Resources\Base;

use App\Http\Resources\BaseResource;

/**
 * @OA\Schema(
 *     title="TeamResource",
 *     description="Team resource",
 *     @OA\Xml(
 *         name="TeamResource"
 *     )
 * )
 */
class TeamBaseResource extends BaseResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var \App\Models\Team[]
     */
    private $data;
    private $with_source_id = false;
    private $with_holder_name = false;
    private $with_result = false;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return parent::toArray($request);
    }

    public function withSourceId() {
        $this->with_source_id = true;

        return $this;
    }

    public function withHolderName() {
        $this->with_holder_name = true;

        return $this;
    }

    public function withResult() {
        $this->with_result = true;

        return $this;
    }

}
