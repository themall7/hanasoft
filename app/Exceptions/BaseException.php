<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator as ValidatorFacade;

class BaseException extends \Exception
{
    protected static $default_message = "";

    public function __construct($message)
    {
        $message_ = config('app.debug') ? $message : self::$default_message;
        parent::__construct($message_);
    }
}
