<?php

namespace App\Exceptions;


class DBException extends BaseException
{
    public function __construct($e)
    {
        parent::$default_message = "Database Transaction Error!";
        parent::__construct($e->getMessage());
    }
}
