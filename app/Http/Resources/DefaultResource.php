<?php

namespace App\Http\Resources;

use App\Http\Traits\ResourceTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class DefaultResource extends JsonResource
{
    use ResourceTrait;
}
