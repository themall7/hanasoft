<?php

namespace App\Http\Resources\Base;

use App\Http\Resources\BaseCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;

class TeamBaseCollection extends BaseCollection
{
    private $with_source_id = false;
    private $with_holder_name = false;
    private $with_result = false;

    public function getCollection()
    {
        return $this->collection;
    }

    public function withSourceId() {
        $this->with_source_id = true;

        return $this;
    }

    public function withHolderName() {
        $this->with_holder_name = true;

        return $this;
    }

    public function withResult() {
        $this->with_result = true;

        return $this;
    }

}
