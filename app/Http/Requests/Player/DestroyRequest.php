<?php

namespace App\Http\Requests\Player;

use App\Http\Requests\ApiRequest;
use Illuminate\Routing\Route;

class DestroyRequest extends ApiRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'id' => 'required|integer',
        ];
    }
    
    public function validationData()
    {
        return array_merge($this->request->all(), [
            'id' => request()->route('id'),
        ]);
    }
}
