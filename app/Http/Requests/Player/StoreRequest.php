<?php

namespace App\Http\Requests\Player;

use App\Http\Requests\ApiRequest;

class StoreRequest extends ApiRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'image_uri' => 'required|string|min:6',
            'team_id' => 'required|integer'
        ];

        return $rules;
    }

}
