<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class ApiRequest extends DefaultRequest
{
    /**
     * @param array $errors
     * @return JsonResponse
     */
    public function response(array $errors) {

        $error_array = [
            'success' => false,
            'message' => implode(";", $errors),
            'data' => (object)[],
        ];
        return new JsonResponse($error_array, 400);
    }
}
