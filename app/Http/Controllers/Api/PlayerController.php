<?php

namespace App\Http\Controllers\Api;

use App\Models\Player;
use App\Models\Team;
use App\Traits\ApiResponser;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Player\DestroyRequest;
use App\Http\Requests\Player\GetByNameRequest;
use App\Http\Requests\Player\ListRequest;
use App\Http\Requests\Player\ShowRequest;
use App\Http\Requests\Player\StoreRequest;
use App\Http\Requests\Player\UpdateRequest;
use App\Http\Resources\Base\PlayerBaseCollection;
use App\Http\Resources\Base\PlayerBaseResource;
use App\Http\Resources\TeamPlayer\TeamPlayerResource;
use App\Http\Resources\TeamPlayer\TeamPlayerCollection;
use App\Http\Resources\DefaultResource;

class PlayerController extends ApiController
{
    use ApiResponser;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
    }
    
    /**
     * @OA\Get(
     *     path="/api/players",
     *     tags={PLAYERS},
     *     summary="Get list of players",
     *     description=IMPLEMENTED,
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function index(ListRequest $request)
    {
        $players = Player::all();
        return new PlayerBaseCollection($players);
    }
    
    /**
     * @OA\Post(
     *     path="/api/players",
     *     tags={PLAYERS},
     *     summary="Store new player",
     *     description=IMPLEMENTED,
     *     @OA\RequestBody(
     *         required=true,
     *         description="Player data",
     *         @OA\JsonContent(
     *             required={"first_name","last_name","image_uri","team_id"},
     *             @OA\Property(property="first_name", type="string", example=""),
     *             @OA\Property(property="last_name", type="string", example=""),
     *             @OA\Property(property="image_uri", type="string", example=""),
     *             @OA\Property(property="team_id", type="integer", example=""),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function store(StoreRequest $request)
    {
        $player = Player::create($request->all());
        
        return new PlayerBaseResource($player);
    }
    
    /**
     * @OA\Get(
     *     path="/api/players/{id}",
     *     tags={PLAYERS},
     *     summary="Get player information",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Player id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function show(ShowRequest $request, int $id)
    {
        $player = Player::find($id);
        
        return new PlayerBaseResource($player);
    }
    
    /**
     * @OA\Get(
     *     path="/api/players/{name}",
     *     tags={PLAYERS},
     *     summary="Get player information with name",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="name",
     *         description="Player name",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function getByName(GetByNameRequest $request, string $name)
    {
        $players = Player::Where('first_name', 'like', "%{$name}%")
            ->orWhere('last_name', 'like', "%{$name}%")->get();
        
        return new PlayerBaseCollection($players);
    }
    
    /**
     * @OA\Put(
     *     path="/api/players/{id}",
     *     tags={PLAYERS},
     *     summary="Update existing player",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Player id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Player data",
     *         @OA\JsonContent(
     *             required={"first_name","last_name","image_uri","team_id"},
     *             @OA\Property(property="first_name", type="string", example=""),
     *             @OA\Property(property="last_name", type="string", example=""),
     *             @OA\Property(property="image_uri", type="string", example=""),
     *             @OA\Property(property="team_id", type="integer", example=""),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=202,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function update(UpdateRequest $request, int $id)
    {
        $player = Player::find($id);
        $player->update($request->all());
        return new PlayerBaseResource($player);
    }
    
    /**
     * @OA\Delete(
     *     path="/api/players/{id}",
     *     tags={PLAYERS},
     *     summary="Get player information",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Player id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function destroy(DestroyRequest $request, int $id)
    {
        $player = Player::find($id);
        $player->delete();
        
        return new DefaultResource(collect(['id' => $id]));
    }
    
    /**
     * @OA\Get(
     *     path="/api/teams/{id}/players",
     *     tags={TEAMS},
     *     summary="Get list of players with team id",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Team id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function showTeamPlayers(ShowRequest $request, int $id)
    {
        $players = Team::with('players')->find($id);
        
        return new TeamPlayerResource($players);
    }
    
    /**
     * @OA\Get(
     *     path="/api/teams/{name}/players",
     *     tags={TEAMS},
     *     summary="Get list of players with team name",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="name",
     *         description="Team name",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function getTeamPlayersByName(GetByNameRequest $request, string $name)
    {
        $players = Team::with('players')->where(['name' => $name])->get();
        
        return new TeamPlayerCollection($players);
    }
    
}