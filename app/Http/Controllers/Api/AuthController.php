<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\DBException;
use App\Models\User;
use App\Traits\ApiResponser;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ApiRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;

class AuthController extends ApiController
{
    use ApiResponser;
    
    /**
     * @OA\Post(
     *     path="/api/auth/register",
     *     tags={AUTHENTICATION},
     *     summary="User Register",
     *     description=IMPLEMENTED,
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="Name",
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     description="Email",
     *                     property="email",
     *                     type="email"
     *                 ),
     *                 @OA\Property(
     *                     description="Password",
     *                     property="password",
     *                     type="password",
     *                     format="password"
     *                 ),
     *                 @OA\Property(
     *                     description="Password Confirmation ",
     *                     property="password_confirmation",
     *                     type="password",
     *                     format="password"
     *                 ),
     *                 @OA\Property(
     *                     description="Role",
     *                     property="role",
     *                     type="string",
     *                     example="",
     *                     enum={"api_user", "api_admin"}
     *                 ),
     *                 required={"name","email","password","role"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function register(RegisterRequest $request)
    {
        
        DB::beginTransaction();
        try {
            $user = User::create($request->all());
            $user->assignRole($request->get('role'));
            
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw new DBException($e);
        }
        
        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }
    
    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     tags={AUTHENTICATION},
     *     summary="User Login",
     *     description=IMPLEMENTED,
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="Email",
     *                     property="email",
     *                     type="email"
     *                 ),
     *                 @OA\Property(
     *                     description="Password",
     *                     property="password",
     *                     type="password",
     *                     format="password"
     *                 ),
     *                 required={"email","password"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->all())) {
            return $this->error('Credentials not match', 401);
        }
        
        $roles = auth()->user()->roles->pluck('name')->toArray();

        return $this->success([
            'token' => auth()->user()->createToken('API Token', $roles)->plainTextToken
        ]);
    }
    
    /**
     * @OA\Post(
     *     path="/api/auth/logout",
     *     tags={AUTHENTICATION},
     *     summary="User Logout",
     *     description=IMPLEMENTED,
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function logout()
    {
        auth()->user()->tokens()->delete();
        
        return $this->success([
            'logout' => true
        ]);
    }
    
    /**
     * @OA\Get(
     *     path="/api/auth/me",
     *     tags={AUTHENTICATION},
     *     summary="User Details",
     *     description=IMPLEMENTED,
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function me(ApiRequest $request)
    {
        $user = auth()->user();
        return $this->success([
            'user' => $user,
            'hasRole' => $user->hasRole('api_admin'),
            'api_admin' => $user->tokenCan('api_admin'),
            'roles' => $user->roles->pluck('name'),
        ]);
    }
    
}