<?php

namespace App\Http\Controllers\Api;

use App\Models\Team;
use App\Traits\ApiResponser;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Team\DestroyRequest;
use App\Http\Requests\Team\GetByNameRequest;
use App\Http\Requests\Team\ListRequest;
use App\Http\Requests\Team\ShowRequest;
use App\Http\Requests\Team\StoreRequest;
use App\Http\Requests\Team\UpdateRequest;
use App\Http\Resources\Base\TeamBaseCollection;
use App\Http\Resources\Base\TeamBaseResource;
use App\Http\Resources\DefaultResource;

class TeamController extends ApiController
{
    use ApiResponser;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
    }
    
    /**
     * @OA\Get(
     *     path="/api/teams",
     *     tags={TEAMS},
     *     summary="Get list of teams",
     *     description=IMPLEMENTED,
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function index(ListRequest $request)
    {
        $teams = Team::all();
        return new TeamBaseCollection($teams);
    }
    
    /**
     * @OA\Post(
     *     path="/api/teams",
     *     tags={TEAMS},
     *     summary="Store new team",
     *     description=IMPLEMENTED,
     *     @OA\RequestBody(
     *         required=true,
     *         description="Team data",
     *         @OA\JsonContent(
     *             required={"name","logo_uri"},
     *             @OA\Property(property="name", type="string", example=""),
     *             @OA\Property(property="logo_uri", type="string", example=""),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function store(StoreRequest $request)
    {
        $team = Team::create($request->all());
        
        return new TeamBaseResource($team);
    }
    
    /**
     * @OA\Get(
     *     path="/api/teams/{id}",
     *     tags={TEAMS},
     *     summary="Get team information",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Team id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function show(ShowRequest $request, int $id)
    {
        $team = Team::find($id);
        
        return new TeamBaseResource($team);
    }
    
    /**
     * @OA\Get(
     *     path="/api/teams/{name}",
     *     tags={TEAMS},
     *     summary="Get team information with name",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="name",
     *         description="Team name",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function getByName(GetByNameRequest $request, string $name)
    {
        $teams = Team::where(['name' => $name])->get();
        
        return new TeamBaseCollection($teams);
    }
    
    /**
     * @OA\Put(
     *     path="/api/teams/{id}",
     *     tags={TEAMS},
     *     summary="Update existing team",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Team id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Team data",
     *         @OA\JsonContent(
     *             required={"name","logo_uri"},
     *             @OA\Property(property="name", type="string", example=""),
     *             @OA\Property(property="logo_uri", type="string", example=""),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=202,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function update(UpdateRequest $request, int $id)
    {
        $team = Team::find($id);
        $team->update($request->all());
        return new TeamBaseResource($team);
    }
    
    /**
     * @OA\Delete(
     *     path="/api/teams/{id}",
     *     tags={TEAMS},
     *     summary="Get team information",
     *     description=IMPLEMENTED,
     *     @OA\Parameter(
     *         name="id",
     *         description="Team id",
     *         required=true,
     *         in="path",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description=RESPONSE_200,
     *     ),
     *     security={{"bearerAuth":{}}},
     * ),
     */
    public function destroy(DestroyRequest $request, int $id)
    {
        $team = Team::find($id);
        $team->delete();
        
        return new DefaultResource(collect(['id' => $id]));
    }
}