<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\ApiRequest;
use App\Http\Resources\DefaultResource;

class VersionController extends ApiController
{
    /**
     * @OA\Get(
     *     path="/api/versions",
     *     tags={APP_VERSION},
     *     summary="Api Version",
     *     description=IMPLEMENTED,
     *     @OA\Response(
     *         response=200,
     *         description=RESPONSE_200,
     *     ),
     * ),
     */
    public function index(ApiRequest $request)
    {
        return new DefaultResource(collect(['version' => '1.0']));
    }
}
