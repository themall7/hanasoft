<?php

namespace App\Http\Controllers;

/**
 * @OA\Swagger(
 *     schemes={"http","https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     basePath="/api/",
 *     @OA\Info(
 *         version="1.0.0",
 *         title="TeamPlayer",
 *         description="This is a api server for TeamPlayer.<hr>

<h2>Implemeted</h2>
<ul>
    <li>Teams APIs</li>
    <li>Players APIs</li>
</ul>",
 *         termsOfService="http://swagger.io/terms/",
 *         @OA\Contact(
 *             email="themall7@gmail.com"
 *         ),
 *         @OA\License(
 *             name="Apache 2.0",
 *             url="http://www.apache.org/licenses/LICENSE-2.0.html",
 *         )
 *     ),
 *     @OA\ExternalDocumentation(
 *         description="Find out more about Swagger",
 *         url="http://swagger.io"
 *     ),
 *
 *     @OA\SecurityScheme(
 *         securityScheme="bearerAuth",
 *         type="http",
 *         scheme="bearer"
 *     ),
 * 
 * )
 */

class ApiController extends Controller
{
}